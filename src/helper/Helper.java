package helper;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Helper {

    public static Boolean createFile(String fileName, Integer size, Integer min, Integer max) {
        File file = new File(fileName);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file, false);
            for (int i = 0; i < size; i++) {
                Integer numberOfDigits = getRandomInteger(min, max);
                for (int j = 0; j < numberOfDigits; j++) {
                    if (j == 0) {
                        fileWriter.write(getRandomInteger(1, 9).toString());
                    } else {
                        fileWriter.write(getRandomInteger(0, 9).toString());
                    }
                }

                fileWriter.write("\n");
            }

            fileWriter.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return false;
        }

        return true;
    }

    public static Boolean writeToFile(String fileName, String text) {
        File file = new File(fileName);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file, false);
            fileWriter.write(text);
            fileWriter.write("\n");
            fileWriter.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return false;
        }

        return true;
    }

    public static List<String> readFromFile(String fileName) {
        File file = new File(fileName);
        List<String> lines = new ArrayList<>();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            String text;
            while ((text = bufferedReader.readLine()) != null) {
                lines.add(text);
            }

            bufferedReader.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        return lines;
    }

    public static Boolean equalFiles(String fileName1, String fileName2) {
        File file1 = new File(fileName1);
        File file2 = new File(fileName2);
        try {
            BufferedReader bufferedReader1 = new BufferedReader(new FileReader(file1));
            BufferedReader bufferedReader2 = new BufferedReader(new FileReader(file2));
            String text1;
            String text2;
            while ((text1 = bufferedReader1.readLine()) != null && (text2 = bufferedReader2.readLine()) != null) {
                if (!text1.equals(text2)) {
                    bufferedReader1.close();
                    bufferedReader2.close();
                    return false;
                }
            }

            bufferedReader1.close();
            bufferedReader2.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return false;
        }

        return true;
    }

    public static Boolean appendToExcel(List<String> items, String fileName) {
        File file = new File(fileName);
        try {
            FileWriter fileWriter = new FileWriter(file, true);
            String prevItem = null;
            for (String item : items) {
                if (prevItem != null) {
                    fileWriter.write(prevItem);
                    fileWriter.write(", ");
                }

                prevItem = item;
            }

            if (prevItem != null) {
                fileWriter.write(prevItem);
                fileWriter.write("\n");
            }

            fileWriter.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return false;
        }

        return true;
    }

    private static Integer getRandomInteger(Integer min, Integer max) {
        return (new Random()).nextInt((max - min) + 1) + min;
    }
}
