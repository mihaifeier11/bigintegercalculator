import bigintegercalculator.BigIntegerCalculator;
import helper.Helper;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        String fileName = "filename.txt";
        String result1 = "result1.txt";
        String result2 = "result2.txt";
        String result3 = "result3.txt";
        Integer numberOfDigits = 1;
        int[] result;
        long time1 = 0;
        long time2 = 0;
        long time3 = 0;
        long timeStartTemp;
        long timeEndTemp;

        for (int i = 0; i < 10; i++) {
            Helper.createFile(fileName, 2, numberOfDigits, numberOfDigits);
            List<String> numbers = Helper.readFromFile(fileName);

            int[] number1 = BigIntegerCalculator.stringToList(numbers.get(0));
            int[] number2 = BigIntegerCalculator.stringToList(numbers.get(1));
            BigIntegerCalculator.reverseList(number1);
            BigIntegerCalculator.reverseList(number2);
            timeStartTemp = System.currentTimeMillis();
            result = BigIntegerCalculator.add(number1, number2);
            timeEndTemp = System.currentTimeMillis();
            time1 += timeEndTemp - timeStartTemp;
            BigIntegerCalculator.reverseList(result);
            Helper.writeToFile(result1, BigIntegerCalculator.listToString(result));

            timeStartTemp = System.currentTimeMillis();
            result = BigIntegerCalculator.add(number1, number2, 8);
            timeEndTemp = System.currentTimeMillis();
            time2 += timeEndTemp - timeStartTemp;
            BigIntegerCalculator.reverseList(result);
            Helper.writeToFile(result2, BigIntegerCalculator.listToString(result));

            timeStartTemp = System.currentTimeMillis();
            result = BigIntegerCalculator.add(number1, number2, 8, true);
            timeEndTemp = System.currentTimeMillis();
            time3 += timeEndTemp - timeStartTemp;
            BigIntegerCalculator.reverseList(result);
            Helper.writeToFile(result3, BigIntegerCalculator.listToString(result));
        }


        System.out.println(time1);
        System.out.println(time2);
        System.out.println(time3);
//        int[] first = new int[]{9,9,9,9};
//        BigIntegerCalculator.reverseList(first);
//        int[] result = BigIntegerCalculator.add(first, first, 2, true);
//        BigIntegerCalculator.reverseList(result);
//        System.out.println(BigIntegerCalculator.listToString(result));
    }
}
