package bigintegercalculator;

import java.util.HashMap;
import java.util.Map;

public class BigIntegerCalculator {

    public static int[] add(int[] firstNumber, int[] secondNumber) {
        int maxSize = Math.max(firstNumber.length, secondNumber.length);
        int[] result = new int[maxSize + 1];

        int carry = 0;
        int tempRes;
        for (int i = 0; i < maxSize; i++) {
            if (i >= firstNumber.length) {
                tempRes = secondNumber[i] + carry;
            } else if (i >= secondNumber.length) {
                tempRes = firstNumber[i] + carry;
            } else {
                tempRes = firstNumber[i] + secondNumber[i] + carry;
            }

            result[i] = tempRes % 10;
            carry = tempRes / 10;
        }

        if (carry > 0) {
            result[maxSize] = carry;
        }

        return result;
    }

    public static int[] add(int[] firstNumber, int[] secondNumber, int threadNumber) {
        int maxSize = Math.max(firstNumber.length, secondNumber.length);
        int[] result = new int[maxSize + 1];
        int remainder = maxSize % threadNumber;
        Map<Integer, Integer> carry = new HashMap<>();

        Thread[] threads = new Thread[threadNumber];
        int start = 0;
        int finish;
        for (int i = 0; i < threadNumber; i++) {
            finish = start + maxSize / threadNumber + (remainder > 0 ? 1 : 0);
            remainder -= 1;
            threads[i] = new BigIntegerAddWorker(firstNumber, secondNumber, result, carry, start, finish);
            threads[i].start();
            start = finish;
        }

        for (int i = 0; i < threads.length; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }

        int minIndex = maxSize + 2;

        for (int value : carry.keySet()) {
            if (value < minIndex) {
                minIndex = value;
            }
        }

        int tempCarry = 0;
        for (int i = minIndex; i < maxSize + 1; i++) {
            result[i] = result[i] + tempCarry + (carry.containsKey(i) ? 1 : 0);
        }

        return result;
    }

    public static int[] add(int[] firstNumber, int[] secondNumber, int threadNumber, boolean carryParallel) {
        int maxSize = Math.max(firstNumber.length, secondNumber.length);
        int[] result = new int[maxSize + 1];
        int remainder = maxSize % threadNumber;
        Map<Integer, Integer> carry = new HashMap<>();

        Thread[] threads = new Thread[threadNumber];
        int start = 0;
        int finish;
        for (int i = 0; i < threadNumber; i++) {
            finish = start + maxSize / threadNumber + (remainder > 0 ? 1 : 0);
            remainder -= 1;
            threads[i] = new BigIntegerAddWorker(firstNumber, secondNumber, result, carry, start, finish);
            threads[i].start();
            start = finish;
        }

        for (int i = 0; i < threads.length; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }

        int previous = -1;
        int i = -1;
        int newMaxSize = carry.size();
        threads = new Thread[newMaxSize];
        for (int current : carry.keySet()) {
            if (previous != -1) {
                threads[i] = new BigIntegerCarryWorker(result, carry, previous, current);
                threads[i].start();
            }

            previous = current;
            i++;
        }

        if (newMaxSize > 0) {
            threads[i] = new BigIntegerCarryWorker(result, carry, previous, maxSize + 1);
            threads[i].start();
        }

        for (i = 0; i < newMaxSize; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }

        return result;
    }

    public static void reverseList(int[] list) {
        int temp;
        for (int i = 0; i < list.length / 2; i++) {
            temp = list[i];
            list[i] = list[list.length - 1 - i];
            list[list.length - 1 - i] = temp;
        }
    }

    public static String listToString(int[] list) {
        boolean firstDigit = true;
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < list.length; i++) {
            if (!firstDigit || list[i] != 0) {
                result.append(list[i]);
                firstDigit = false;
            }
        }

        return result.toString();
    }

    public static int[] stringToList(String string) {
        int[] result = new int[string.length()];

        for (int i = 0; i < string.length(); i++) {
            result[i] = string.charAt(i) - 48;
        }

        return result;
    }

}
