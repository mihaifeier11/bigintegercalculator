package bigintegercalculator;

import java.util.Map;

public class BigIntegerAddWorker extends Thread {
    private int[] firstNumber;
    private int[] secondNumber;
    private int[] result;
    private Map<Integer, Integer> finalCarry;
    private int start;
    private int finish;

    public BigIntegerAddWorker(int[] firstNumber, int[] secondNumber, int[] result,
                               Map<Integer, Integer> finalCarry, int start, int finish) {
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
        this.result = result;
        this.finalCarry = finalCarry;
        this.start = start;
        this.finish = finish;
    }

    @Override
    public void run() {
        int carry = 0;

        int tempRes;
        for (int i = start; i < finish; i++) {
            if (i >= firstNumber.length) {
                tempRes = secondNumber[i] + carry;
            } else if (i >= secondNumber.length) {
                tempRes = firstNumber[i] + carry;
            } else {
                tempRes = firstNumber[i] + secondNumber[i] + carry;
            }

            result[i] = tempRes % 10;
            carry = tempRes / 10;
        }

        if (carry > 0) {
            synchronized (finalCarry) {
                finalCarry.put(finish, carry);
            }
        }
    }
}
