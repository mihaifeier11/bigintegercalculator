package bigintegercalculator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BigIntegerCarryWorker extends Thread {
    private int[] result;
    private Map<Integer, Integer> finalCarry;
    private int start;
    private int finish;

    public BigIntegerCarryWorker(int[] result, Map<Integer, Integer> finalCarry, int start, int finish) {
        this.result = result;
        this.finalCarry = finalCarry;
        this.start = start;
        this.finish = finish;
    }

    @Override
    public void run() {
        int carry = 0;
        int temp;

        for (int i = start; i < finish; i++) {
            temp = result[i] + carry + (finalCarry.containsKey(i) ? 1 : 0);
            result[i] = temp % 10;
            carry = temp / 10;
        }

        if (result.length != finish) {
            result[finish] = result[finish] + carry;
        }
    }
}
